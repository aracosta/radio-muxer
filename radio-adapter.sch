EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:radio-adapter-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Adattatore TB7100"
Date "2016-01-14"
Rev "2"
Comp "distelco s.r.l."
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L DB25 J1
U 1 1 5696DA56
P 2200 3750
F 0 "J1" H 2250 5100 70  0000 C CNN
F 1 "RADIO" H 2150 2400 70  0000 C CNN
F 2 "Connect:DB25MC" H 2200 3750 60  0001 C CNN
F 3 "" H 2200 3750 60  0000 C CNN
	1    2200 3750
	-1   0    0    -1  
$EndComp
$Comp
L RJ45 J2
U 1 1 5696DA5D
P 6950 3700
F 0 "J2" H 7150 4200 60  0000 C CNN
F 1 "RJ45" H 6800 4200 60  0000 C CNN
F 2 "Connect:RJ45_8" H 6950 3700 60  0001 C CNN
F 3 "" H 6950 3700 60  0000 C CNN
	1    6950 3700
	0    1    1    0   
$EndComp
$Comp
L GND #PWR01
U 1 1 5696DA70
P 5950 4100
F 0 "#PWR01" H 5950 3850 50  0001 C CNN
F 1 "GND" H 5950 3950 50  0000 C CNN
F 2 "" H 5950 4100 60  0000 C CNN
F 3 "" H 5950 4100 60  0000 C CNN
	1    5950 4100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2650 4950 3300 4950
Wire Wire Line
	2650 4350 3300 4350
Wire Wire Line
	2650 4150 3300 4150
Wire Wire Line
	5950 4050 6500 4050
Wire Wire Line
	5600 3450 6500 3450
Wire Wire Line
	5600 3850 6500 3850
Wire Wire Line
	5600 3350 6500 3350
Wire Wire Line
	2850 2550 2650 2550
Wire Wire Line
	3300 4850 2650 4850
Wire Wire Line
	2650 4650 3300 4650
Wire Wire Line
	5950 3950 5950 4100
$Comp
L GND #PWR02
U 1 1 5697B3B4
P 2850 2550
F 0 "#PWR02" H 2850 2300 50  0001 C CNN
F 1 "GND" H 2850 2400 50  0000 C CNN
F 2 "" H 2850 2550 60  0000 C CNN
F 3 "" H 2850 2550 60  0000 C CNN
	1    2850 2550
	0    -1   1    0   
$EndComp
Wire Wire Line
	6500 3650 5600 3650
Wire Wire Line
	5600 3750 6500 3750
NoConn ~ 2650 2750
NoConn ~ 2650 2850
NoConn ~ 2650 2950
NoConn ~ 2650 3050
NoConn ~ 2650 3150
NoConn ~ 2650 3250
NoConn ~ 2650 3350
NoConn ~ 2650 3750
NoConn ~ 2650 3850
NoConn ~ 2650 3950
NoConn ~ 2650 4050
NoConn ~ 2650 4550
NoConn ~ 2650 4750
NoConn ~ 7300 4250
Wire Wire Line
	2650 3550 3300 3550
Wire Wire Line
	6500 3550 5600 3550
Wire Wire Line
	2650 2650 3500 2650
Text Label 6150 3450 0    60   ~ 0
aOut+
Text Label 6150 3650 0    60   ~ 0
TxOn
Text Label 6150 3750 0    60   ~ 0
RxGate
Text Label 6150 3350 0    60   ~ 0
aOut-
Text Label 6150 3850 0    60   ~ 0
aIn+
Text Label 6150 3550 0    60   ~ 0
aIn-
Text Label 2750 4950 0    60   ~ 0
aOut+
Text Label 2750 4350 0    60   ~ 0
aOut-
Text Label 2750 3550 0    60   ~ 0
aIn-
Text Label 2750 4150 0    60   ~ 0
aIn+
Text Label 2750 4850 0    60   ~ 0
RxGate
Text Label 2750 4650 0    60   ~ 0
TxOn
$Comp
L R R1
U 1 1 569DF31D
P 3650 2650
F 0 "R1" V 3730 2650 50  0000 C CNN
F 1 "3K3" V 3650 2650 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 3580 2650 30  0001 C CNN
F 3 "" H 3650 2650 30  0000 C CNN
	1    3650 2650
	0    1    1    0   
$EndComp
$Comp
L LED D1
U 1 1 569DF3A4
P 4150 2650
F 0 "D1" H 4150 2750 50  0000 C CNN
F 1 "LED" H 4150 2550 50  0000 C CNN
F 2 "LEDs:LED-5MM" H 4150 2650 60  0001 C CNN
F 3 "" H 4150 2650 60  0000 C CNN
	1    4150 2650
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR03
U 1 1 569DF402
P 4550 2650
F 0 "#PWR03" H 4550 2400 50  0001 C CNN
F 1 "GND" H 4550 2500 50  0000 C CNN
F 2 "" H 4550 2650 60  0000 C CNN
F 3 "" H 4550 2650 60  0000 C CNN
	1    4550 2650
	0    -1   1    0   
$EndComp
Wire Wire Line
	3800 2650 3950 2650
Wire Wire Line
	4350 2650 4550 2650
NoConn ~ 2650 4250
NoConn ~ 2650 4450
Wire Wire Line
	6500 3950 5950 3950
Connection ~ 5950 4050
$Comp
L CONN_01X01 TP1
U 1 1 56A7AEB5
P 5450 4900
F 0 "TP1" H 5450 5000 50  0000 C CNN
F 1 "CONN_01X01" V 5550 4900 50  0001 C CNN
F 2 "w_pin_strip:pin_strip_1" H 5450 4900 60  0001 C CNN
F 3 "" H 5450 4900 60  0000 C CNN
	1    5450 4900
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR04
U 1 1 56A7AF2D
P 5450 5150
F 0 "#PWR04" H 5450 4900 50  0001 C CNN
F 1 "GND" H 5450 5000 50  0000 C CNN
F 2 "" H 5450 5150 60  0000 C CNN
F 3 "" H 5450 5150 60  0000 C CNN
	1    5450 5150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5450 5150 5450 5100
NoConn ~ 2650 3450
NoConn ~ 2650 3650
$EndSCHEMATC
