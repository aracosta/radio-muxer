EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:xnet-adapter-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "xnet-adapter"
Date "2016-01-19"
Rev "2"
Comp "distelco s.r.l."
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RJ45 J3
U 1 1 569DF995
P 7350 2500
F 0 "J3" H 7550 3000 60  0000 C CNN
F 1 "CC" H 7200 3000 60  0000 C CNN
F 2 "Connect:RJ45_8" H 7350 2500 60  0001 C CNN
F 3 "" H 7350 2500 60  0000 C CNN
	1    7350 2500
	0    1    1    0   
$EndComp
$Comp
L RJ45 J4
U 1 1 569DFAED
P 7350 3600
F 0 "J4" H 7550 4100 60  0000 C CNN
F 1 "AUDIO1" H 7200 4100 60  0000 C CNN
F 2 "Connect:RJ45_8" H 7350 3600 60  0001 C CNN
F 3 "" H 7350 3600 60  0000 C CNN
	1    7350 3600
	0    1    1    0   
$EndComp
$Comp
L RJ45 J5
U 1 1 569DFB47
P 7350 4700
F 0 "J5" H 7550 5200 60  0000 C CNN
F 1 "AUDIO2" H 7200 5200 60  0000 C CNN
F 2 "Connect:RJ45_8" H 7350 4700 60  0001 C CNN
F 3 "" H 7350 4700 60  0000 C CNN
	1    7350 4700
	0    1    1    0   
$EndComp
$Comp
L RJ45 J1
U 1 1 569DFC75
P 3950 3050
F 0 "J1" H 4150 3550 60  0000 C CNN
F 1 "RD1" H 3800 3550 60  0000 C CNN
F 2 "Connect:RJ45_8" H 3950 3050 60  0001 C CNN
F 3 "" H 3950 3050 60  0000 C CNN
	1    3950 3050
	0    -1   1    0   
$EndComp
$Comp
L RJ45 J2
U 1 1 569DFD39
P 3950 4200
F 0 "J2" H 4150 4700 60  0000 C CNN
F 1 "RD2" H 3800 4700 60  0000 C CNN
F 2 "Connect:RJ45_8" H 3950 4200 60  0001 C CNN
F 3 "" H 3950 4200 60  0000 C CNN
	1    3950 4200
	0    -1   1    0   
$EndComp
Text Label 6800 2150 2    60   ~ 0
Out2
Text Label 6800 2350 2    60   ~ 0
Out1
Text Label 6800 2550 2    60   ~ 0
In1
Text Label 6800 2650 2    60   ~ 0
In2
$Comp
L GND #PWR01
U 1 1 569E014D
P 5600 5400
F 0 "#PWR01" H 5600 5150 50  0001 C CNN
F 1 "GND" H 5600 5250 50  0000 C CNN
F 2 "" H 5600 5400 60  0000 C CNN
F 3 "" H 5600 5400 60  0000 C CNN
	1    5600 5400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6900 2150 6350 2150
Wire Wire Line
	6900 2250 5600 2250
Wire Wire Line
	6900 2350 6350 2350
Wire Wire Line
	6900 2450 5600 2450
Wire Wire Line
	6900 2550 6350 2550
Wire Wire Line
	6900 2650 6350 2650
Wire Wire Line
	6900 2750 5600 2750
Wire Wire Line
	6900 2850 5600 2850
Wire Wire Line
	6900 3250 6350 3250
Wire Wire Line
	6900 3350 6350 3350
Wire Wire Line
	6900 3450 6350 3450
Wire Wire Line
	6900 3750 6350 3750
Wire Wire Line
	5600 3850 6900 3850
Wire Wire Line
	5600 3950 6900 3950
Wire Wire Line
	5600 4950 6900 4950
Wire Wire Line
	5600 5050 6900 5050
Wire Wire Line
	4950 3850 4400 3850
Wire Wire Line
	4950 3950 4400 3950
Wire Wire Line
	4950 4050 4400 4050
Wire Wire Line
	4400 4150 4950 4150
Wire Wire Line
	4950 4250 4400 4250
Wire Wire Line
	4950 4350 4400 4350
Wire Wire Line
	5600 4450 4400 4450
Wire Wire Line
	5600 4550 4400 4550
Wire Wire Line
	4950 2700 4400 2700
Wire Wire Line
	4950 2800 4400 2800
Wire Wire Line
	4950 2900 4400 2900
Wire Wire Line
	4400 3000 4950 3000
Wire Wire Line
	4950 3100 4400 3100
Wire Wire Line
	4950 3200 4400 3200
Wire Wire Line
	5600 3300 4400 3300
Wire Wire Line
	5600 3400 4400 3400
Wire Wire Line
	5600 2250 5600 5400
Connection ~ 5600 2850
Connection ~ 5600 3850
Connection ~ 5600 3950
Connection ~ 5600 4950
Connection ~ 5600 5050
Text Label 6850 3250 2    60   ~ 0
aOut1-
Text Label 6850 3350 2    60   ~ 0
aOut1+
Text Label 6850 3450 2    60   ~ 0
aIn1-
Text Label 6800 3750 2    60   ~ 0
aIn1+
NoConn ~ 6900 3550
NoConn ~ 6900 3650
Wire Wire Line
	6900 4350 6350 4350
Wire Wire Line
	6900 4450 6350 4450
Wire Wire Line
	6900 4550 6350 4550
Wire Wire Line
	6900 4850 6350 4850
Text Label 6850 4350 2    60   ~ 0
aOut2-
Text Label 6850 4450 2    60   ~ 0
aOut2+
Text Label 6850 4550 2    60   ~ 0
aIn2-
Text Label 6800 4850 2    60   ~ 0
aIn2+
NoConn ~ 6900 4650
NoConn ~ 6900 4750
Connection ~ 5600 3300
Connection ~ 5600 3400
Connection ~ 5600 4450
Connection ~ 5600 4550
Text Label 4800 2700 2    60   ~ 0
aIn1-
Text Label 4800 2800 2    60   ~ 0
aIn1+
Text Label 4800 2900 2    60   ~ 0
aOut1-
Text Label 4800 3200 2    60   ~ 0
aOut1+
Connection ~ 5600 2750
Connection ~ 5600 2450
Text Label 4800 3850 2    60   ~ 0
aIn2-
Text Label 4800 3950 2    60   ~ 0
aIn2+
Text Label 4700 3000 2    60   ~ 0
Out1
Text Label 4700 3100 2    60   ~ 0
In1
Text Label 4800 4050 2    60   ~ 0
aOut2-
Text Label 4800 4350 2    60   ~ 0
aOut2+
Text Label 4750 4150 2    60   ~ 0
Out2
Text Label 4750 4250 2    60   ~ 0
In2
Text Notes 8450 3600 0    60   ~ 0
X-Net connector side
Text Notes 2050 3600 0    60   ~ 0
Radio adapter side
Wire Wire Line
	5650 5300 5600 5300
Connection ~ 5600 5300
$Comp
L CONN_01X01 TP1
U 1 1 569E1A40
P 5850 5300
F 0 "TP1" H 5850 5400 50  0000 C CNN
F 1 "T" H 5950 5300 50  0000 C CNN
F 2 "w_pin_strip:pin_strip_1" H 5850 5300 60  0001 C CNN
F 3 "" H 5850 5300 60  0000 C CNN
	1    5850 5300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
