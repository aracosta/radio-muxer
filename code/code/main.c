/*
 * muxer.c
 *
 * Created: 12/01/2016 09:58:29
 * Author : matteoc
 */ 

# define F_CPU 4000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <string.h>

#define UART_TX_BUFFER_SIZE 512
#define UART_BAUD_RATE 9600
#include "uart.h"


typedef struct {
	long timer;				// timer counter to decrement, if -1 don't count
	unsigned char elapsed;	// timer elapsed flag
} S_timers;


#define DEBUG_TIMER 0	// timer used for serial communication with Bluetooth
#define LED_TIMER 1	// timer used for led blinking
#define MAX_TIMERS	4
volatile S_timers sw_timers[MAX_TIMERS];
volatile unsigned long millis;
volatile unsigned char scaler;

#define DEBUG_FREQ	2000	// communicate every x ms

//
// occur every 250us
// keep the software timers running
//
ISR(TIMER0_COMP_vect)
{
	int i;
	
	scaler = (scaler+1)%4;	// every 4 interrupt count 1 ms
	if (scaler==0)
	millis++;
	
	for (i=0; i<MAX_TIMERS; i++)
	{
		if (sw_timers[i].timer > 0 )
		{
			sw_timers[i].timer--;
			sw_timers[i].elapsed = 0;
		}
		else
		{
			sw_timers[i].elapsed = 1;
		}
	}
}
void get_ms(unsigned long *count)
{
	*count = millis;
}
void swTimer_clear(int timer)
{
	sw_timers[timer].timer = 0;
	sw_timers[timer].elapsed = 0;
}
void swTimer_start(int timer, long delay)
{
	sw_timers[timer].timer = delay*4;
	sw_timers[timer].elapsed = 0;
}
inline int swTimer_elapsed(int timer)
{
	return sw_timers[timer].elapsed;
}



const char m_ON[] PROGMEM  = {"ON"};
const char m_OFF[] PROGMEM  = {"OFF"};
const char m_CRLF[] PROGMEM  = {"\n\r"};

#define LED		(1<<PIND7)
#define LED_ON	(PORTD |= LED)
#define LED_OFF	(PORTD &= ~LED)

// define muxing modes from external selector
#define MODE_T3_INVALID	3
#define MODE_T3_LOCAL	0
#define MODE_T3_E1		1
#define MODE_T3_E2		2
#define MODE_SEL_A _BV(PIND2)
#define MODE_SEL_B _BV(PIND3)

#define T1_RX _BV(PINC0)
#define T2_RX _BV(PINC1)
#define T3_RX _BV(PINC2)
#define T4_RX _BV(PINC3)
#define E1_RX _BV(PINC4)
#define E2_RX _BV(PINC5)
#define E4_RX _BV(PINC6)
#define PTT _BV(PINC7)

#define T1_RX_ON	((PINC & T1_RX) == 0)
#define T2_RX_ON	((PINC & T2_RX) == 0)
#define T3_RX_ON	((PINC & T3_RX) == 0)
#define T4_RX_ON	((PINC & T4_RX) == 0)
#define E1_RX_ON	((PINC & E1_RX) == 0)
#define E2_RX_ON	((PINC & E2_RX) == 0)
#define E4_RX_ON	((PINC & E4_RX) == 0)

#define T1_TX _BV(PINB0)
#define T2_TX _BV(PINB1)
#define T3_TX _BV(PINB2)
#define T4_TX _BV(PINB3)
#define E1_TX _BV(PIND4)
#define E2_TX _BV(PIND5)
#define E4_TX _BV(PIND6)

#define T1_TX_OFF	(PORTB |= T1_TX);
#define T1_TX_ON	(PORTB &= ~T1_TX);
#define T2_TX_OFF	(PORTB |= T2_TX);
#define T2_TX_ON	(PORTB &= ~T2_TX);
#define T3_TX_OFF	(PORTB |= T3_TX);
#define T3_TX_ON	(PORTB &= ~T3_TX);
#define T4_TX_OFF	(PORTB |= T4_TX);
#define T4_TX_ON	(PORTB &= ~T4_TX);
#define E1_TX_OFF	(PORTD |= E1_TX)
#define E1_TX_ON	(PORTD &= ~E1_TX)
#define E2_TX_OFF	(PORTD |= E2_TX)
#define E2_TX_ON	(PORTD &= ~E2_TX)
#define E4_TX_OFF	(PORTD |= E4_TX)
#define E4_TX_ON	(PORTD &= ~E4_TX)



#define SEL_E1_A0	_BV(PINA0)
#define SEL_E2_A0	_BV(PINA1)
#define SEL_T3_A0	_BV(PINA2)
#define SEL_T3_A1	_BV(PINA3)
#define SEL_FM_A0	_BV(PINA4)
#define GL_ENA		_BV(PINA7)


char current_mode;

void print_status()
{
	
	uart_puts_p(PSTR("Input status: \n\r"));
	uart_puts_p(PSTR("T1 Rx - "));
	if (T1_RX_ON) {
		uart_puts_p(m_ON);
	} else {
		uart_puts_p(m_OFF);		
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(PSTR("T2 Rx - "));
	if (PINC & T2_RX) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(PSTR("T3 Rx - "));
	if (PINC & T3_RX) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(PSTR("T4 Rx - "));
	if (PINC & T4_RX) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(PSTR("E1 Rx - "));
	if (PINC & E1_RX) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(PSTR("E2 Rx - "));
	if (PINC & E2_RX) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(PSTR("E4 Rx - "));
	if (PINC & E4_RX) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(PSTR("PTT   - "));
	if (PINC & PTT) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);

	uart_puts_p(PSTR("Output status: \n\r"));
	uart_puts_p(PSTR("T1 Tx - "));
	if (PINB & T1_TX) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(PSTR("T2 Tx - "));
	if (PINB & T2_TX) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(PSTR("T3 Tx - "));
	if (PINB & T3_TX) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(PSTR("T4 Tx - "));
	if (PINB & T4_TX) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(PSTR("E1 Tx - "));
	if (PIND & E1_TX) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(PSTR("E2 Tx - "));
	if (PIND & E2_TX) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(PSTR("E4 Tx - "));
	if (PIND & E4_TX) {
		uart_puts_p(m_OFF);
	} else {
		uart_puts_p(m_ON);
	}
	uart_puts_p(m_CRLF);
	
	uart_puts_p(PSTR("Muxer status: \n\r"));
	uart_puts_p(PSTR("Selector status: "));
	switch (current_mode)
	{
		case MODE_T3_LOCAL:
			uart_puts_p(PSTR("T3 local mode"));
			break;	
		case MODE_T3_E1:
			uart_puts_p(PSTR("T3 <-> E1"));
			break;
		case MODE_T3_E2:
			uart_puts_p(PSTR("T3 <-> E2"));
			break;
		default:		
			uart_puts_p(PSTR("Invalid"));
			break;
	}
	uart_puts_p(m_CRLF);
	uart_puts_p(m_CRLF);
	
	return;
}


int main(void)
{
	// initialize ports
	DDRA = 0xff;
	PORTA = 0;
	
	DDRB = 0x0f;
	PORTB = 0x0f;
	
	DDRC = 0;
	PORTC = 0xff;	// pullup active
	
	DDRD = 0xf3;
	PORTD = 0xfc;	// pullup active on input
	
	// Timer0 is used for internal software timers, It's configured for 16uS frequency and interrupt every 256us.
	// initialize timer0
	// CTC mode
	// Internal Clock
	// prescaler 64
	// interrupt on compare match
	// TODO: calcolare i nuovi valori timer
	TCNT0 = 0;
	OCR0 = 16;
	TIMSK = 0b00000010;
	TCCR0 = 0b00001011;
		
	// initialize serial port
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU));
	_delay_ms(100);

	// init interrupt
	sei();
	
	uart_puts_p(PSTR("Radio Muxer firmware v0.9\n\r"));
	
	PORTA |= GL_ENA;
	
	swTimer_start(DEBUG_TIMER, DEBUG_FREQ);
	
    while (1) 
    {
		if ((PIND & (MODE_SEL_A|MODE_SEL_B)) == 0) {
			// invalid mode fallback to local
			current_mode = MODE_T3_INVALID;
		} else if ((PIND & (MODE_SEL_A|MODE_SEL_B)) == (MODE_SEL_A|MODE_SEL_B)) {
			current_mode = MODE_T3_LOCAL;			
		} else if ((PIND & MODE_SEL_A) == MODE_SEL_A) {
			current_mode = MODE_T3_E1;
		} else if ((PIND & MODE_SEL_B) == MODE_SEL_B) {
			current_mode = MODE_T3_E2;
		}
		LED_ON;
		_delay_ms(10);
		LED_OFF;
		_delay_ms(10);
		
		
		
		// logica muxer 
	
		// E1 -> T1
		if(E1_RX_ON)
		{
			T1_TX_ON;
		} else {
			T1_TX_OFF;
		}
		// E2 -> T2
		if(E2_RX_ON)
		{
			T2_TX_ON;
		} else {
			T2_TX_OFF;
		}
		// E4 -> T4
		if(E4_RX_ON)
		{
			T4_TX_ON;
		} else {
			T4_TX_OFF;
		}
		// T4 -> E4
		if(T4_RX_ON)
		{
			E4_TX_ON;
		} else {
			E4_TX_OFF;
		}
		
		// -> T3
		if (T3_RX_ON || ((current_mode==MODE_T3_E1)&& E1_RX_ON) || ((current_mode==MODE_T3_E2)&& E2_RX_ON))
		{
			T3_TX_ON;
		} else {
			T3_TX_OFF;
		}
		
		// -> E1
		if (T1_RX_ON || ((current_mode==MODE_T3_E1)&& T3_RX_ON))
		{
			E1_TX_ON;
		} else {
			E1_TX_OFF;
		}
		
		// -> E2
		if (T2_RX_ON || ((current_mode==MODE_T3_E2)&& T3_RX_ON))
		{
			E2_TX_ON;
		} else {
			E2_TX_OFF;
		}
		
		if (swTimer_elapsed(DEBUG_TIMER))
		{
			swTimer_start(DEBUG_TIMER, DEBUG_FREQ);
			print_status();		
		}

    }
}

